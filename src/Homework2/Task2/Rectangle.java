package Homework2.Task2;

import static java.lang.Math.sqrt;

/**
 *
 * @author Julia Misiura
 */
public class Rectangle extends Triangle implements Shape {

    private double sideA;
    private double sideB;

    public Rectangle(double sideA, double sideB) {
        super(sideA, sideB, sqrt(sideA * sideA + sideB * sideB));
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public double calculatePerimeter(double sideA, double sideB) {
        return 2 * (sideA + sideB);
    }

    public double calculateArea(double sideA, double sideB) {
        return sideA * sideB;
    }

    @Override
    public String toString() {
        return "a = " + sideA + "\nb = " + sideB + "\n"
                + "Perimeter: " + calculatePerimeter(sideA, sideB) + "\n"
                + "Area: " + calculateArea(sideA, sideB) + "\n"
                + "2 equivalent triangles: \n"
                + super.toString();
    }

    @Override
    public double getSideA() {
        return sideA;
    }

    @Override
    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    @Override
    public double getSideB() {
        return sideB;
    }

    @Override
    public void setSideB(double sideB) {
        this.sideB = sideB;
    }

}
