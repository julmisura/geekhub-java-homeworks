package Homework2.Task2;

/**
 *
 * @author Julia Misiura
 */
public enum EnumShapes {
    CIRCLE, SQUARE, RECTANGLE, TRIANGLE
}
