package Homework2.Task2;

import static java.lang.Math.PI;

/**
 *
 * @author Julia Misiura
 */
public class Circle implements Shape {

    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * PI * radius;
    }

    @Override
    public double calculateArea() {
        return PI * radius * radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Perimeter: " + calculatePerimeter() + "\n"
                + "Area: " + calculateArea() + "\n";
    }

}
