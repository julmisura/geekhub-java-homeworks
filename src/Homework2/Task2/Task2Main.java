package Homework2.Task2;

import static java.lang.Math.abs;
import java.util.Scanner;

/**
 *
 * @author Julia Misiura
 */
public class Task2Main {

    public static void main(String[] args) {
        System.out.println("Enter name of shape: ");
        Scanner in = new Scanner(System.in);
        String shapeName = (in.next()).toUpperCase();
        System.out.println("If you type a negative number, "
                + "absolute value will be taken!");
        try {
            EnumShapes shape = EnumShapes.valueOf(shapeName);
            switch (shape) {
                case CIRCLE:
                    System.out.println("Enter radius: ");
                    Circle circle = new Circle(abs(in.nextDouble()));
                    System.out.println(circle);
                    break;
                case SQUARE:
                    System.out.println("Enter side: ");
                    Square square = new Square(abs(in.nextDouble()));
                    System.out.println(square);
                    break;
                case RECTANGLE:
                    System.out.println("Enter side a: ");
                    double side_a = in.nextDouble();
                    System.out.println("Enter side b: ");
                    double side_b = in.nextDouble();
                    Rectangle rectangle = new Rectangle(abs(side_a), abs(side_b));
                    System.out.println(rectangle);
                    break;
                case TRIANGLE:
                    System.out.println("Enter side a: ");
                    side_a = in.nextDouble();
                    System.out.println("Enter side b: ");
                    side_b = in.nextDouble();
                    System.out.println("Enter side c: ");
                    double side_c = in.nextDouble();
                    Triangle triangle = new Triangle(abs(side_a),
                            abs(side_b), abs(side_c));
                    System.out.println(triangle);
                    break;
            }
        } catch (IllegalArgumentException e) {
            System.out.println("ERROR!");
        }
    }

}
