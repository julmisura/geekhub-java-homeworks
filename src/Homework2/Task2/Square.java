package Homework2.Task2;

/**
 *
 * @author Julia Misiura
 */
public class Square extends Rectangle implements Shape {

    private double side;
    public Square(double side) {
        super(side, side);
        this.side = side;
    }

    public double calculatePerimeter(double side) {
        return 4 * side;
    }

    public double calculateArea(double side) {
        return side * side;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

}
