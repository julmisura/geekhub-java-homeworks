package Homework2.Task2;

/**
 *
 * @author Julia Misiura
 */
public interface Shape {

    double calculatePerimeter();

    double calculateArea();

}
