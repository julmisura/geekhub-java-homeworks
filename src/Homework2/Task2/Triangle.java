package Homework2.Task2;

import static java.lang.Math.sqrt;

/**
 *
 * @author Julia Misiura
 */
public class Triangle implements Shape {

    private double sideA;
    private double sideB;
    private double sideC;

    public Triangle(double sideA, double sideB, double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    private boolean validateTriangle(double sideA, double sideB, double sideC) {
        return !((sideA == 0 || sideB == 0 || sideC == 0)
                || (sideA + sideB <= sideC) || (sideB + sideC <= sideA)
                || (sideA + sideC <= sideB));
    }

    @Override
    public double calculatePerimeter() {
        if (!validateTriangle(sideA, sideB, sideC)) {
            return 0;
        }
        return sideA + sideB + sideC;
    }

    @Override
    public double calculateArea() {
        if (!validateTriangle(sideA, sideB, sideC)) {
            System.out.println("ERROR! Not a triangle!");
            return 0;
        }
        double p = calculatePerimeter() / 2;
        return sqrt(p * (p - sideA) * (p - sideB) * (p - sideC));
    }

    @Override
    public String toString() {
        return "a = " + sideA + "\nb = " + sideB
                + "\nc = " + sideC + "\n"
                + "Perimeter: " + calculatePerimeter() + "\n"
                + "Area: " + calculateArea() + "\n";
    }

    public double getSideA() {
        return sideA;
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public void setSideB(double sideB) {
        this.sideB = sideB;
    }

    public double getSideC() {
        return sideC;
    }

    public void setSideC(double sideC) {
        this.sideC = sideC;
    }

}
