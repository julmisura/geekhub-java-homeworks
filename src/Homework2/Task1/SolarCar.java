package Homework2.Task1;

/**
 *
 * @author Julia Misiura
 */
public class SolarCar extends Car {
    
    private final SolarEngine solarEngine;

    public SolarCar() {
        super();
        solarEngine = new SolarEngine();
    }

    @Override
    public void accelerate() {
        super.accelerate();
        solarEngine.empowerSolarPanels();
    }

    @Override
    public void brake() {
        super.brake();
        solarEngine.disableSolarPanels();
    }

    @Override
    public void printSpeed() {
        super.printSpeed(); 
        System.out.println("Current power of solar panels: " + 
                solarEngine.getPowerOfSolarPanels() + " kw\n");
    }

}
