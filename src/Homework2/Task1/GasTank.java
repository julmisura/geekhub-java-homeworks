package Homework2.Task1;

/**
 *
 * @author Julia Misiura
 */
public class GasTank {

    private double amountFuel;

    public GasTank() {
        this.amountFuel = 100;
    }

    public double getAmountFuel() {
        return amountFuel;
    }

    public void setAmountFuel(double amountFuel) {
        this.amountFuel = amountFuel;
    }

}
