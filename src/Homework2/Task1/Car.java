package Homework2.Task1;

/**
 *
 * @author Julia Misiura
 */
public class Car extends Vehicle {

    private final Wheels wheels;

    public Car() {
        wheels = new Wheels();
    }

    @Override
    public void turn(String direction) {
        super.turn(direction);
        wheels.turnWheels(direction);
    }

    @Override
    public void brake() {
        super.brake();
        wheels.stopWheels();
    }

    @Override
    public void accelerate() {
        super.accelerate();
        wheels.hasteWheels();
    }

}
