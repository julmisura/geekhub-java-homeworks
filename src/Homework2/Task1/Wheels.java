package Homework2.Task1;

/**
 *
 * @author Julia Misiura
 */
public class Wheels {

    void turnWheels(String direction) {
        System.out.println("Wheels are turned "
                + direction + " successfully!");
    }

    void stopWheels() {
        System.out.println("Wheels are stopped successfully!");
    }

    void hasteWheels() {
        System.out.println("Wheels are turning faster now!");
    }

}
