package Homework2.Task1;

/**
 *
 * @author Julia Misiura
 */
public class Vehicle implements Driveable {

    private final Engine engine;

    public Vehicle() {
        engine = new Engine();
    }

    @Override
    public void accelerate() {
        engine.enhance();
        System.out.println(this.getClass().getSimpleName()
                + " is going faster now!");
    }

    @Override
    public void brake() {
        engine.stop();
        System.out.println(this.getClass().getSimpleName()
                + " stopped successfully!");
    }

    @Override
    public void turn(String direction) {
        if (engine.getSpeed() == 0 || engine.getAmountOfFuel() == 0) {
            System.out.println("Can't turn!");
            return;
        }
        System.out.println(this.getClass().getSimpleName()
                + " turned " + direction + " successfully!");
    }

    public void printAmountOfFuel() {
        System.out.println("Fuel left: " + engine.getAmountOfFuel());
    }
    
    public void printSpeed() {
        System.out.println("Current speed: " + engine.getSpeed());
    }

}
