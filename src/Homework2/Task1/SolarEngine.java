package Homework2.Task1;

/**
 *
 * @author Julia Misiura
 */
public class SolarEngine extends Engine {

    private double powerOfSolarPanels;

    public SolarEngine() {
        powerOfSolarPanels = 0;
    }

    public void empowerSolarPanels() {
        System.out.println("Solar panels are empowered successfully!");
        powerOfSolarPanels += 0.1;
        System.out.println("Current power of solar panels: "
                + powerOfSolarPanels + " kW");
    }

    public void disableSolarPanels() {
        System.out.println("Solar panels are disabled successfully!");
        powerOfSolarPanels = 0;
    }

    public double getPowerOfSolarPanels() {
        return powerOfSolarPanels;
    }

    public void setPowerOfSolarPanels(double powerOfSolarPanels) {
        this.powerOfSolarPanels = powerOfSolarPanels;
    }

}
