package Homework2.Task1;

/**
 *
 * @author Julia Misiura
 */
public interface Driveable {

    void accelerate();

    void brake();

    void turn(String direction);

}
