package Homework2.Task1;

/**
 *
 * @author Julia Misiura
 */
public class Task1Main {

    public static void main(String[] args) {
        Vehicle car = new Car();
        Vehicle boat = new Boat();
        Car solarCar = new SolarCar();
        car.accelerate();
        car.turn("right");
        car.brake();
        boat.accelerate();
        boat.turn("left");
        boat.brake();
        solarCar.accelerate();
        solarCar.turn("right");
        solarCar.brake();
    }

}
