package Homework2.Task1;

/**
 *
 * @author Julia Misiura
 */
public class Engine {

    private final GasTank gasTank;
    private double speed;

    public Engine() {
        gasTank = new GasTank();
        speed = 0;
    }

    void stop() {
        if (speed == 0) {
            System.out.println("");
        }
    }

    void enhance() {
        double fuel = getAmountOfFuel();
        if (fuel == 0) {
            System.out.println("No fuel!");
            return;
        }
        gasTank.setAmountFuel(fuel - 10);
        speed += 10;
    }

    double getAmountOfFuel() {
        return gasTank.getAmountFuel();
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }
    

}
