package Homework1.Task1;

import java.util.Scanner;

/**
 * Puts Fibonacci numbers from 0 to n;
 *
 * @author Julia Misiura
 */
public class Homework1_Task1 {

    public static void main(String[] args) {
        int fibo0 = 0;
        int fibo1 = 1;
        int fiboPrev = fibo1;
        int fiboPrevPrev = fibo0; //previous to previous Fib number
        int fiboCurrent = fiboPrev + fiboPrevPrev;
        System.out.print("Enter N: ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if (n < 0) {
            System.out.println("There can be only non-negative numbers!");
            return;
        }
        if (n == 0) {
            System.out.println(fibo0);
            return;
        }
        System.out.print(fibo0 + " " + fibo1);
        while (fiboCurrent <= n) {
            System.out.print(" " + fiboCurrent);
            fiboPrevPrev = fiboPrev;
            fiboPrev = fiboCurrent;
            fiboCurrent = fiboPrev + fiboPrevPrev;
        }
        System.out.println();
    }

}
