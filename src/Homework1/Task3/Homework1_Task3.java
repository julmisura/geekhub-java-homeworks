package Homework1.Task3;


/**
 * RLE algorithm
 *
 * @author Shadowalker
 */
public class Homework1_Task3 {

    public static void main(String[] args) {
        System.out.println("Command line argument: " + args[0]);
        int length = args[0].length();
        char[] str = args[0].toCharArray();
        char curchar = str[0];
        int cnt = 1;
        for (int i = 1; i < length; i++) {
            if (str[i] == curchar) {
                cnt++;
            } else {
                System.out.print(cnt);
                System.out.print(curchar);
                cnt = 1;
                curchar = str[i];
            }
        }
        System.out.print(cnt);
        System.out.println(curchar);
    }
}
