package Homework1.Task2;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Calculates n! (factorial of n).
 *
 * @author Julia Misiura
 */
public class Homework1_Task2 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter N");
        String str = in.nextLine();
        if (str.isEmpty()) {
            System.out.println("ERROR: empty input!");
            return;
        }
        try {
            int n = Integer.parseInt(str);
            if (n < 0) {
                System.out.println("ERROR: there can be only "
                        + "non-negative numbers!");
                return;
            }
            if (n >= 10) {
                System.out.println("This operation may take "
                        + "some time. Proceed anyway? Yes - 1, "
                        + "No - 0");
                int answer = in.nextInt();
                if (answer == 0) {
                    return;
                }
            }
            System.out.println(calcFactorial(n));
        } catch (NumberFormatException e) {
            System.out.println("ERROR: Wrong input!");
        }
    }

    static BigInteger calcFactorial(int n) {
        BigInteger fact = BigInteger.ONE;
        for (int i = 2; i <= n; i++) {
            fact = fact.multiply(BigInteger.valueOf(i));
        }
        System.out.print("N!: ");
        return fact;
    }
}
