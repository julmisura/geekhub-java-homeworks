package Homework4.Task2;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author Julia Misiura
 */
public class TaskOrganizer implements TaskManager {

    private final List<Task> taskList;

    public TaskOrganizer() {
        taskList = new ArrayList<>();
    }

    @Override
    public void addTask(Calendar date, Task task) {
        taskList.add(task);
        task.setDate(date);
    }

    @Override
    public void removeTask(Calendar date) {
        for (Iterator<Task> it = taskList.iterator(); it.hasNext();) {
            Task currentTask = it.next();
            if (currentTask.getDate().equals(date)) {
                it.remove();
            }
        }
    }

    @Override
    public Collection<String> getCategories() {
        System.out.println("List of categories: ");
        Collection<String> categories = new HashSet<>();
        for (Task currentTask : taskList) {
            categories.add(currentTask.getCategory());
        }
        return categories;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        System.out.println("Tasks by categories:");
        Map<String, List<Task>> tasksByCategories = new HashMap<>();
        for (Task currentTask : taskList) {
            if (!tasksByCategories.containsKey(currentTask.getCategory())) {
                tasksByCategories.put(currentTask.getCategory(), new ArrayList<>());
            }
            tasksByCategories.get(currentTask.getCategory()).add(currentTask);
        }
        DateComparator comp = new DateComparator();
        for (Entry entry : tasksByCategories.entrySet()) {
            ArrayList<Task> list = (ArrayList<Task>) entry.getValue();
            list.sort(comp);
        }
        return tasksByCategories;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        System.out.println("Tasks of category " + category + ":");
        List<Task> tasksByCategory = new ArrayList<>();
        for (Task currentTask : taskList) {
            if (currentTask.getCategory().equals(category)) {
                tasksByCategory.add(currentTask);
            }
        }
        DateComparator comp = new DateComparator();
        tasksByCategory.sort(comp);
        return tasksByCategory;
    }

    @Override
    public List<Task> getTasksForToday() {
        System.out.println("Tasks for today:");
        List<Task> tasksForToday = new ArrayList<>();
        Calendar today = new GregorianCalendar();
        int todayDay = today.get(Calendar.DATE);
        int todayMonth = today.get(Calendar.MONTH);
        int todayYear = today.get(Calendar.YEAR);
        for (Task currentTask : taskList) {
            int currentDay = currentTask.getDate().get(Calendar.DATE);
            int currentMonth = currentTask.getDate().get(Calendar.MONTH);
            int currentYear = currentTask.getDate().get(Calendar.YEAR);
            if (todayDay == currentDay && todayMonth == currentMonth 
                    && todayYear == currentYear) {
                tasksForToday.add(currentTask);
            }
        }
        return tasksForToday;
    }

}