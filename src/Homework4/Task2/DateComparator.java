package Homework4.Task2;

import java.util.Comparator;

/**
 *
 * @author Julia Misiura
 */
public class DateComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        return ((Task) o1).getDate().compareTo(((Task) o2).getDate());
    }

}


