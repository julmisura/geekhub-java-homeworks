package Homework4.Task2;

import java.util.Calendar;

/**
 *
 * @author Julia Misiura
 */
public class Task {

    private String category;
    private String description;
    private Calendar date;

    public Task(String category, String description) {
        this.category = category;
        this.description = description;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "description=" + description + "; date="
                + date.get(Calendar.DATE) + " " + (date.get(Calendar.MONTH) + 1)
                + " " + date.get(Calendar.YEAR);
    }

}
