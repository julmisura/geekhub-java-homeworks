package Homework4.Task2;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Julia Misiura
 */
public interface TaskManager {

    public void addTask(Calendar date, Task task);

    public void removeTask(Calendar date);

    public Collection<String> getCategories();

    public Map<String, List<Task>> getTasksByCategories();

    public List<Task> getTasksByCategory(String category);

    public List<Task> getTasksForToday();
}

