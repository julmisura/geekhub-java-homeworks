package Homework4.Task2;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author Julia Misiura
 */
public class Task2Main {

    public static void main(String[] args) {
        TaskOrganizer taskOrganizer = new TaskOrganizer();
        taskOrganizer.addTask(new GregorianCalendar(2017, Calendar.NOVEMBER, 6), 
                new Task("GeekHub", "Do training task"));
        taskOrganizer.addTask(new GregorianCalendar(), 
                new Task("GeekHub", "Commit this task"));
        taskOrganizer.addTask(new GregorianCalendar(2017, Calendar.NOVEMBER, 6), 
                new Task("University", "Do Linear Algebra"));
        taskOrganizer.addTask(new GregorianCalendar(), 
                new Task("University", "Do Program Engineering"));
        taskOrganizer.removeTask(new GregorianCalendar());
        System.out.println(taskOrganizer.getCategories());
        System.out.println(taskOrganizer.getTasksByCategories());
        System.out.println(taskOrganizer.getTasksByCategory("GeekHub"));
        System.out.println(taskOrganizer.getTasksByCategory("University"));
        System.out.println(taskOrganizer.getTasksForToday());
    }

}