package Homework4.Task1;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Julia Misiura
 * @param <T>
 */
public class SetOperator<T> implements SetOperations {

    private final Set<T> setA;
    private final Set<T> setB;

    public SetOperator(Set setA, Set setB) {
        this.setA = setA;
        this.setB = setB;
    }

    @Override
    public boolean equals(Set a, Set b) {
        return a.containsAll(b);
    }

    @Override
    public Set union(Set a, Set b) {
        Set<T> aTemp = new HashSet<>();
        aTemp.addAll(a);
        aTemp.addAll(b);
        return aTemp;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set<T> aTemp = new HashSet<>();
        aTemp.addAll(a);
        aTemp.removeAll(b);
        return aTemp;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set<T> aTemp = new HashSet<>();
        aTemp.addAll(a);
        aTemp.retainAll(b);
        return aTemp;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        Set<T> aTemp = new HashSet<>();
        aTemp.addAll(a);
        Set<T> bTemp = new HashSet<>();
        bTemp.addAll(b);
        aTemp.removeAll(b);
        bTemp.removeAll(a);
        aTemp.addAll(bTemp);
        return aTemp;
    }

}
