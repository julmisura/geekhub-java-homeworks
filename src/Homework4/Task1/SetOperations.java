package Homework4.Task1;

import java.util.Set;

/**
 *
 * @author Julia Misiura
 */
public interface SetOperations {

    public boolean equals(Set a, Set b);

    public Set union(Set a, Set b);

    public Set subtract(Set a, Set b);

    public Set intersect(Set a, Set b);

    public Set symmetricSubtract(Set a, Set b);

}
