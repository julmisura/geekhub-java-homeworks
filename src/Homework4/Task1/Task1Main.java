package Homework4.Task1;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Julia Misiura
 */
public class Task1Main {

    public static void main(String[] args) {
        Set<String> setA = new HashSet<>();
        Set<String> setB = new HashSet<>();
        setA.add("Hello");
        setA.add("World");
        setB.add("Hello");
        setB.add("Java rules!");
        setB.add("Rush B");
        SetOperator setOperator = new SetOperator(setA, setB);
        System.out.println("Set A: " + setA);
        System.out.println("Set B: " + setB + "\n");
        System.out.println("A = B: " + setOperator.equals(setA, setB));
        System.out.println("A U B: " + setOperator.union(setA, setB));
        System.out.println("AB: " + setOperator.intersect(setA, setB));
        System.out.println("A \\ B: " + setOperator.subtract(setA, setB));
        System.out.println("A Δ B: " + setOperator.symmetricSubtract(setA, setB));
    }

}
