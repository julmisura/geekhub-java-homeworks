package Homework3.Task3;

import Homework3.Task2.LinkedList;

class Entry<K, V> {
    private K key;
    private V value;

    public Entry(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Entry<?, ?> entry = (Entry<?, ?>) o;

        return key.equals(entry.key);
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }
}

/**
 * @param <K>
 * @param <V>
 * @author Julia Misiura
 */
public class Map<K, V> {

    LinkedList<Entry<K, V>> entries;

    public Map() {
        entries = new LinkedList<>();
    }

    public boolean put(K key, V value) {
        for (int i = 0; i < entries.size(); i++) {
            if (key.equals(entries.get(i).getKey())) {
                entries.get(i).setValue(value);
                return true;
            }
        }
        return entries.add(new Entry<>(key, value));
    }

    public V get(K key) {
        Entry<K, V> current = entries.get(0);
        while (!(current.getKey().equals(key))) {
            current = entries.next();
        }
        return current.getValue();
    }

    public int size() {
        return entries.size();
    }

    public void remove(K key) {
        int cnt = 0;
        try {
            Entry<K, V> current = entries.get(0);
            while (!(current.getKey().equals(key))) {
                current = entries.next();
                cnt++;
            }
            entries.remove(cnt);
            entries.setSize(entries.size() - 1);
        } catch (NullPointerException exc) {

        }
    }

    LinkedList<K> getKeys() {
        LinkedList<K> keyList = new LinkedList<>();
        try {
            for (int i = 0; i < entries.size(); i++) {
                keyList.add(entries.get(i).getKey());
            }
        } catch (NullPointerException exc) {
            return keyList;
        }
        return keyList;
    }
}
