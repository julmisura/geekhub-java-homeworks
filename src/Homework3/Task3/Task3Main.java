/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Homework3.Task3;

import Homework3.Task2.LinkedList;

/**
 * @author Julia Misiura
 */
public class Task3Main {

    public static void main(String[] args) {
        Map map = new Map();
        map.put(2.5, "Hello");
        map.put("ABC", 5);
        map.put(-8, "abc");
        map.put("ABC", "DEF");
        map.remove("1");
        map.remove(-8);
        LinkedList keyList = map.getKeys();
        System.out.println("Size: " + map.size());
        for (int i = 0; i < map.size(); i++) {
            System.out.println(keyList.get(i) + " "
                    + map.get(keyList.get(i)));
        }
    }

}
