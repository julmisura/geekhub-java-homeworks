package Homework3.Task1;

import java.util.Random;
import java.util.stream.Collectors;

/**
 *
 * @author Julia Misiura
 */
public class RandomString {

    private final int size;
    private String string = "";
    private StringBuffer stringBuf;
    private StringBuilder stringBuild;

    public RandomString(int size) {
        this.size = size;
        stringBuf = new StringBuffer(size);
        stringBuild = new StringBuilder(size);
    }

    public void generateStrings() {
        int length = size;
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "abcdefghijklmnopqrstuvwxyz"
                + "0123456789";
        string = new Random().ints(length, 0, chars.length())
                .mapToObj(i -> "" + chars.charAt(i))
                .collect(Collectors.joining());
        stringBuf = new StringBuffer(string);  
        stringBuild = new StringBuilder(string); 
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public StringBuffer getStringBuf() {
        return stringBuf;
    }

    public void setStringBuf(StringBuffer stringBuf) {
        this.stringBuf = stringBuf;
    }

    public StringBuilder getStringBuild() {
        return stringBuild;
    }

    public void setStringBuild(StringBuilder stringBuild) {
        this.stringBuild = stringBuild;
    }

}
