package Homework3.Task1;

/**
 *
 * @author Julia Misiura
 */
public class Task1Main {

    public static void main(String[] args) {
        RandomString randomString = new RandomString(10);
        String[] arrString = new String[10000];
        StringBuffer[] arrStringBuffer = new StringBuffer[10000];
        StringBuilder[] arrStringBuilder = new StringBuilder[10000];
        for (int i = 0; i < 10000; i++) {
            randomString.generateStrings();
            arrString[i] = randomString.getString();
            arrStringBuffer[i] = randomString.getStringBuf();
            arrStringBuilder[i] = randomString.getStringBuild();
        }
        long time1 = System.currentTimeMillis();
        //String
        String string = "";
        for (int i = 0; i < 10000; i++) {
            string += arrString[i];
        }
        long time2 = System.currentTimeMillis();
        long timeString = time2 - time1;
        //StringBuffer
        time1 = System.currentTimeMillis();
        StringBuffer stringBuffer = new StringBuffer(100000);
        for (int i = 0; i < 10000; i++) {
            stringBuffer.append(arrStringBuffer[i]);
        }
        time2 = System.currentTimeMillis();
        long timeBuf = time2 - time1;
        //StringBuilder
        time1 = System.currentTimeMillis();
        StringBuilder stringBuilder = new StringBuilder(100000);
        for (int i = 0; i < 10000; i++) {
            stringBuilder.append(arrStringBuilder[i]);
        }
        time2 = System.currentTimeMillis();
        long timeBuild = time2 - time1;
        //Diagram
        System.out.println("^           " + timeString + "          " + timeBuf
                + "         " + timeBuild);
        for (int i = 8; i >= 0; i--) {
            String row = "|            ";
            if (timeString >= 100 * i) {
                row += "|           ";
            } else {
                row += "            ";
            }
            if (timeBuf >= 100 * i) {
                row += "|           ";
            } else {
                row += "            ";
            }
            if (timeBuild >= 100 * i) {
                row += "|           ";
            } else {
                row += "            ";
            }
            System.out.println(row);
        }
        System.out.println("__________String__StringBuffer__StringBuilder_>");
    }

}
