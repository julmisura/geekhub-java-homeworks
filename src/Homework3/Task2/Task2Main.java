package Homework3.Task2;

/**
 *
 * @author Julia Misiura
 */
public class Task2Main {

    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        list.add(8);
        list.put(true, 1);
        list.put("Hello, world!", 0);
        list.put(2.5, 1);
        list.remove(2);
        System.out.println("Size: " + list.size());
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.next());
        }
    }

}
