package Homework3.Task2;

/**
 * @param <T>
 */
class Node<T> {

    private T element;
    private Node next;
    private Node prev;

    public Node(T element) {
        this.element = element;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node getPrev() {
        return prev;
    }

    public void setPrev(Node prev) {
        this.prev = prev;
    }

    public T getElement() {
        return element;
    }

    public void setElement(T element) {
        this.element = element;
    }
}

/**
 * @param <T>
 * @author Julia Misiura
 */
public class LinkedList<T> {

    private Node header;
    private Node current;
    private int size;
    private int cnt;

    public LinkedList() {
        this.header = new Node(null);
        this.current = header;
        size = 0;
        cnt = 0;
    }

    public T next() {
        try {
            if (cnt == 0) {
                current = header.getNext();
                cnt++;
                return (T) current.getElement();
            }
            cnt++;
            current = current.getNext();
            return (T) current.getElement();
        } catch (NullPointerException exc) {
            return null;
        }
    }

    public T prev() {
        if (cnt == 0) {
            current = header.getPrev();
            cnt++;
            return (T) current.getElement();
        }
        cnt++;
        current = current.getPrev();
        return (T) current.getElement();
    }

    public boolean add(T element) {
        if (element == null) {
            return false;
        }
        Node newElement = new Node(element);
        if (size == 0) {
            newElement.setPrev(header);
            header.setNext(newElement);
        } else {
            this.get(size - 1);
            newElement.setPrev(current);
            current.setNext(newElement);
        }
        header.setPrev(newElement);
        current = newElement;
        size++;
        return true;
    }

    public boolean put(T e, int index) {
        if (index < 0 || index > size) {
            return false;
        }
        try {
            if (index == size) {
                return this.add(e);
            }
            Node newElement = new Node(e);
            this.get(index - 1);
            Node currentPrev = current;
            this.get(index);
            Node currentNext = current;
            newElement.setPrev(currentPrev);
            currentPrev.setNext(newElement);
            newElement.setNext(currentNext);
            currentNext.setPrev(newElement);
            size++;
            return true;
        } catch (NullPointerException exc) {
            return false;
        }
    }

    public T get(int index) {
        if (index > size) {
            System.out.println("Element doesn't exist!");
            return null;
        }
        try {
            current = header;
            for (int i = 0; i <= index; i++) {
                current = current.getNext();
            }
            return (T) current.getElement();
        } catch (NullPointerException exc) {
            return null;
        }
    }

    public boolean remove(int index) {
        if (index < 0 || index > size) {
            return false;
        }
        try {
            this.get(index - 1);
            Node prev = current;
            Node next = prev.getNext().getNext();
            prev.setNext(next);
            next.setPrev(prev);
            size--;
            return true;
        } catch (NullPointerException exc) {
            return false;
        }
    }

    public int size() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
